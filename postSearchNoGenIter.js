const faker = require('faker');
const mongoose = require('mongoose');
const { Name } = require('./nameModel');
const { inspect } = require('util');
const maxElems = process.argv[2] || 100;

class MicroRI {
  #config;

  #__funcMerge = (funcArray) => {
    return async (doc, cb) => {
      var result = doc;
      for (var func of funcArray) {
        result = await new Promise((resolve) => {
          func(result, resolve);
        });

        if (!result) return cb(result);
      }

      return cb(result);
    };
  };

  #iterateCursor = async (cursor, limit, func) => {
    var partial = [];

    try {
      let doc = await cursor.next();

      while (doc) {
        if (limit > 0) {
          var result = await new Promise((resolve) => {
            func(doc, resolve);
          });

          if (result) {
            limit--;
            partial.push(result);
          }

          doc = await cursor.next();
        } else {
          return partial;
        }
      }

      return partial;
    } catch (err) {
      return partial;
    }
  };

  #__getCounts = async (query) => {
    var totalCount = await Name.estimatedDocumentCount();

    var queryCount = query;
    if (queryCount.skip) {
      delete queryCount.skip;
    }

    if (queryCount.limit) {
      delete queryCount.limit;
    }

    var searchCount = await Name.countDocuments(queryCount);

    return {
      count: totalCount,
      searchCount,
    };
  };

  async handlePost(query, projection, options) {
    try {
      var effectiveOpts = Object.assign({}, options);
      var limit;

      if (this.#config.postSearch) {
        limit = options.limit;
        delete effectiveOpts.limit;
      }

      const queryExec = Name.find(query, projection, effectiveOpts);
      var cursor = queryExec.cursor();

      if (this.#config.postSearch) {
        var acum = [];

        acum = await this.#iterateCursor(
          cursor,
          limit || Infinity,
          this.#__funcMerge(this.#config.postSearch)
        );

        try {
          cursor.close();
        } catch {}

        return Promise.resolve({
          status: await this.#__getCounts(query, options),
          result: acum,
        });
      } else {
        return new Promise(async (resolve, reject) => {
          try {
            resolve({
              status: await this.#__getCounts(query, options),
              result: await queryExec.exec(),
            });
          } catch (err) {
            reject(err);
          }
        });
      }
    } catch (err) {
      cursor.close();
      console.error('Error post: ', err);
      throw err;
    }
  }

  constructor(config) {
    this.#config = config;
  }
}

async function init() {
  mongoose.connect('mongodb://localhost:27017/postSearch', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  var dbCount = await Name.estimatedDocumentCount();
  if (dbCount < maxElems) {
    for (let i = 0; i < maxElems - dbCount; i++) {
      await Name.create({ name: faker.name.firstName() });
    }
  }
}

const tests = [];

tests.push(async function testNoPost() {
  let config = {};
  const RI = new MicroRI(config);
  let query = {
    skip: 13,
    limit: 30,
  };

  let result = await RI.handlePost({}, { _id: 0, __v: 0 }, query);

  console.log(
    `Query without postSearch: \nQuery: ${inspect(
      query,
      false,
      null
    )} \nResult: ${inspect(result, false, null)} \nResult length: ${
      result.result.length
    }`
  );
});

tests.push(async function testPost() {
  let config = {
    postSearch: [
      function (elem, next) {
        elem.name += '-step-1';
        next(elem);
      },
      function (elem, next) {
        elem.name += '-step-2';
        next(elem);
      },
    ],
  };
  const RI = new MicroRI(config);
  let query = {
    skip: 13,
    limit: 30,
  };

  let result = await RI.handlePost({}, { _id: 0, __v: 0 }, query);

  console.log(
    `\nQuery with postSearch: \nQuery: ${inspect(
      query,
      false,
      null
    )} \nResult: ${inspect(result, false, null)} \nResult length: ${
      result.result.length
    }`
  );
});

tests.push(async function testPost() {
  let config = {
    postSearch: [
      function (elem, next) {
        elem.name += '-no-limit-1';
        next(elem);
      },
      function (elem, next) {
        elem.name += '-no-limit-2';
        next(elem);
      },
    ],
  };
  const RI = new MicroRI(config);
  let query = {
    skip: 13,
  };

  let result = await RI.handlePost({}, { _id: 0, __v: 0 }, query);

  console.log(
    `\nQuery with postSearch (no limit): \nQuery: ${inspect(
      query,
      false,
      null
    )} \nResult: ${inspect(result, false, null)} \nResult length: ${
      result.result.length
    }`
  );
});

tests.push(async function testPostRemove() {
  let config = {
    postSearch: [
      function (elem, next) {
        if (elem.name.startsWith('A')) {
          next(false);
        } else {
          elem.name += '-no-A';
          next(elem);
        }
      },
      function (elem, next) {
        if (elem.name.startsWith('N')) {
          next(false);
        } else {
          elem.name += '-no-N';
          next(elem);
        }
      },
    ],
  };

  const RI = new MicroRI(config);
  let query = {
    skip: 13,
    limit: 30,
  };

  let result = await RI.handlePost({}, { _id: 0, __v: 0 }, query);

  console.log(
    `\nQuery with postSearch (remove): \nQuery: ${inspect(
      query,
      false,
      null
    )} \nResult: ${inspect(result, false, null)} \nResult length: ${
      result.result.length
    }`
  );
});

async function run() {
  try {
    await init();
    for (let test of tests) {
      let tstamp = Date.now();
      await test();
      console.log(`Elapsed: ${Date.now() - tstamp} ms`);
    }
    process.exit(0);
  } catch (err) {
    console.error('Test error: ', err);
    process.exit(-1);
  }
}

run();
